program bdswin7;

uses
  Vcl.Forms, Vcl.Dialogs,
  System.Win.Registry,
  Winapi.Windows;

{$R *.res}
procedure Main;
var
  ARegistry: TRegistry;
  bdsPath: string;
begin
  ARegistry := TRegistry.Create;
  ARegistry.RootKey := HKEY_CURRENT_USER;
  ARegistry.Access := KEY_WOW64_64KEY or KEY_ALL_ACCESS;
  bdsPath := '';
  if ARegistry.OpenKey('Software\Embarcadero\BDS\19.0', False) then
  begin
    bdsPath := ARegistry.ReadString('App');
  end;
  ARegistry.Destroy;

  if bdsPath = '' then
  begin
    ShowMessage('û�ҵ�Delphi XE10.2!');
    Exit;
  end;

  ARegistry := TRegistry.Create;
  ARegistry.RootKey := HKEY_CURRENT_USER;
  ARegistry.Access := KEY_WOW64_64KEY or KEY_ALL_ACCESS;
  if ARegistry.OpenKey('Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers', False) then
  begin
    ARegistry.WriteString(bdsPath, '~ WIN7RTM');
  end;
  ARegistry.Destroy;
  WinExec(PAnsiChar(AnsiString('"' + bdsPath + '" -pDelphi')), SW_SHOWDEFAULT);
end;

begin
  Main;
end.

