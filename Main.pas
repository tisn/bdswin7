unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    edt1: TEdit;
    btn1: TButton;
    btn2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  System.Win.Registry;
{$R *.dfm}

procedure TForm1.btn2Click(Sender: TObject);
var
  ARegistry: TRegistry;
  s: string;
  rs: PAnsiChar;
begin
  ARegistry := TRegistry.Create;
  ARegistry.RootKey := HKEY_CURRENT_USER;
  ARegistry.Access := KEY_WOW64_64KEY or KEY_ALL_ACCESS;
  if ARegistry.OpenKey('Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers', False) then
  begin
    ARegistry.WriteString(edt1.Text, '~ WIN7RTM');
  end;
  ARegistry.Destroy;
  WinExec(PAnsiChar(AnsiString('"' + edt1.Text + '" -pDelphi')), SW_SHOWDEFAULT);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  ARegistry: TRegistry;
begin
  ARegistry := TRegistry.Create;
  ARegistry.RootKey := HKEY_CURRENT_USER;
  ARegistry.Access := KEY_WOW64_64KEY or KEY_ALL_ACCESS;
  if ARegistry.OpenKey('Software\Embarcadero\BDS\19.0', False) then
  begin
    edt1.Text := ARegistry.ReadString('App');
  end;
  ARegistry.Destroy;
end;

end.

